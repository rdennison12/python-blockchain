Section Summary: Preparing the Blockchain for Collaboration

Great job on completing this section on preparing the blockchain for collaboration!

Here's a list of the main topics that appeared in the section:

    Chain validation is the process of ensuring that the data of an external blockchain is formatted correctly. For the blockchain to be valid, there are multiple rules to enforce. For starters, every block must be valid, with a proper hash based on the block fields, correctly adjusted difficulty, acceptable number of leading 0's in the hash for the proof of work requirement, and more. Likewise, in the blockchain itself must start with the genesis block, and every block's last hash must reference the hash of the block that came before it.

    Chain replacement is the process of substituting the current blockchain data for the data of an incoming blockchain. If the incoming blockchain is longer, and valid, then it should replace the current blockchain. This will allow a valid blockchain, with new blocks, to spread across the eventual blockchain network, becoming the true blockchain that all nodes in the blockchain network agree upon.

Also, here's the completed source code at this point in the course:

https://github.com/15Dkatz/python-blockchain-tutorial/tree/4399e7f56b8f5a38b59b712c2c7a556e1c2a4989

***

With chain validation and replacement completed, we've now prepared the blockchain for collaboration. So, in the next section, let's actually create the blockchain network that participants of the system will use to collaborate!