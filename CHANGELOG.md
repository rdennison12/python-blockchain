# Blockchain in Python, JS, and React

## Commits
* Initial commit - Project and Repo created
* Add .gitignore file
* Refactored blockchain.py into separate modules
  * Created method to generate an initial block
* Begin building the hash algorithm
* Completed the crypto_hash module
* Added requirements.txt and README.md with commands
* Refactored project: created packages for the current files 
* Begin building the tests for the current modules
* Completed building the tests for the current modules
* Begin introducing "Difficulty" and "Nonce" values
* Created static method to convert hexadecimal to binary
* Implemented hex_to_binary method that converts hexadecimal to binary
* Refactored section-summaries
* Added is_valid_block method and created validation tests
* Created chain validation method and the necessary tests
* Begin to build the network for creating new blocks in the blockchain