"""
 Created by Rick Dennison
 Date:      12/11/21
 File Name: __init__.py
 Project:   python-blockchain-2021
"""
import os
import requests
import random

from flask import Flask, jsonify
from backend.blockchain.blockchain import Blockchain

app = Flask(__name__)
blockchain = Blockchain()


@app.route('/')
def route_default():
    return 'Welcome to the blockchain'


@app.route('/blockchain')
def route_blockchain():
    return jsonify(blockchain.to_json())


@app.route('/blockchain/mine')
def route_blockchain_mine():
    transaction_data = 'stubbed_transaction_data'
    blockchain.add_block(transaction_data)
    return jsonify(blockchain.chain[-1].to_json())
    # block = blockchain.chain[-1]
    # pubsub.broadcast_block(block)
    # return jsonify(block.to_json())


app.run()
