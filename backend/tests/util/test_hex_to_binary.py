"""
 Created by Rick Dennison
 Date:      12/10/21
 File Name: test_hex_to_binary.py
 Project:   python-blockchain-2021
"""
from backend.util.hex_to_binary import hex_to_binary


def test_hex_to_binary():
    original_number = 789
    hex_number = hex(original_number)[2:]
    binary_number = hex_to_binary(hex_number)

    assert int(binary_number, 2) == original_number
