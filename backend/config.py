"""
 Created by Rick Dennison
 Date:      12/10/21
 File Name: config.py
 Project:   python-blockchain-2021
"""
NANOSECONDS = 1
MICROSECONDS = 1000 * NANOSECONDS
MILLISECONDS = 1000 * MICROSECONDS
SECONDS = 1000 * MILLISECONDS

MINE_RATE = 4 * SECONDS
